const { attach_cookie } = require('./utils/attach-cookie');
const stringReplace = require('string-replace-middleware');
const cookieParser = require('cookie-parser');
const express = require('express');
const path = require('path');
const cors = require('cors');
const app = express();
const config = require('config');
const fs = require('fs');
const ejs = require('ejs');
const uuidv4 = require('uuid/v4');
const msisdn = require('express-msisdn');
const NameAndEmail = require('./constant/name_email');
const PhoneNumber = require('./constant/phone_number');
const Address = require('./constant/address');

app.use(cors());
app.use(msisdn());
app.use(cookieParser());
app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", req.header('origin')); // update to match the domain you will make the request from
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

// attach uuid
// app.use(attach_cookie('/static/tracking.js'));

let trackingFilePath = './dist/tracking.js';
if (process.env.NODE_ENV === 'production') {
	trackingFilePath = './dist/tracking.min.js';
}

app.use('/static/tracking.js', function (req, res) {
	let f = ejs.compile(fs.readFileSync(trackingFilePath).toString('utf8'));
	let fileContent = f({ 
		hostApi        : config.get('hostApi'),
		hostTracking   : config.get('hostTracking'),
		uuid           : req.cookies.uuid || uuidv4(),
		key            : req.query.key,
		scriptTracking : config.get('scriptTracking')
	});
	
	res.setHeader('Content-Type', 'application/javascript');
	res.setHeader('Content-Length', fileContent.length);
	res.send(fileContent);
});

app.get('/static/msisdn', (req, res) =>{ 
	res.send({'msisdn': req.msisdn});
});

app.get('/static/customer-data', (req, res) =>{
	try{
		const lenOfNameAndEmailArray = NameAndEmail.length - 1;
		const lenOfAddressArray = Address.length - 1;
		const lenOfPhoneNumberArray = PhoneNumber.length - 1;
		const randomNameAndEmailIndex = Math.round(Math.random() * lenOfNameAndEmailArray);
		const randomAddressIndex =  Math.round(Math.random() * lenOfAddressArray);
		const randomPhoneNumberIndex =  Math.round(Math.random() * lenOfPhoneNumberArray);
		const data = {
			nameAndEmail: NameAndEmail[randomNameAndEmailIndex],
			address: Address[randomAddressIndex],
			phoneNumber: PhoneNumber[randomPhoneNumberIndex]
		};

		return res.send(data);
	}catch(e){
		console.error(e);
		const data = {
			nameAndEmail:{
				"F.Name": "",
        "L.Name": "",
        "Email": ""
			},
			address:  {
        "Address": ""
    	},
			phoneNumber: {
        "Mobile": ""
    	}
		};

		return res.send(data);
	}
})

// Serving static files
app.use('/static', express.static(path.join(__dirname, 'public')));

module.exports = app;
