const onUnload = async () => {
  if (!logId) {
    console.error('Not have log id');
    return;
  }

  try {
    const url = APIs.timeUnload.replace(':id', logId);
    const body = {
      time: new Date().getTime()
    };

    $.ajax({
      type: 'PUT',
      async: false,
      url: url,
      data: body
    });
  } catch (e) {
    console.log(e);
  }
};