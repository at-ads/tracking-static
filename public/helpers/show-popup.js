const initSendInfoPopup = async (dataSend) => {
  if (dataSend) {
    if (dataSend.data) {
      if (dataSend.data.isPopupOpening) {
        $(document).ready(() => {
          const data = dataSend.data;
          loadSendInfoPopup(data);
          if (dataSend.data.popupConfig) {
            if (dataSend.data.popupConfig.autoShowPopup) {
              const time = dataSend.data.popupConfig.autoShowPopupRepeatTime ? Number(dataSend.data.popupConfig.autoShowPopupRepeatTime) || null : null;
              if (time) {
                setInterval(() => { showSendInfoPopup(); }, time * 60000);
              }
            }
          }
        });

        return;
      }
      return;
    }
    return;
  }
  return;
};

const loadSendInfoPopup = (data) => {
  //Popup container
  let container = document.createElement('div');
  container.setAttribute('id', 'ccclt-send-info-popup__container');
  container.style.display = 'none';
  document.body.appendChild(container);

  //Popup body
  let popup = document.createElement('div');
  popup.setAttribute('id', 'ccclt-send-info-popup');
  container.appendChild(popup);

  //Info dialog
  let infoDialogContainer = document.createElement('div');
  infoDialogContainer.setAttribute('id', 'ccclt-send-info-popup__info-dialog__container');
  infoDialogContainer.style.display = 'none';
  popup.appendChild(infoDialogContainer);

  let infoDialog = document.createElement('div');
  infoDialog.setAttribute('id', 'ccclt-send-info-popup__info-dialog');
  infoDialogContainer.appendChild(infoDialog);

  let infoDialogIcon = document.createElement('img');
  infoDialogIcon.setAttribute('id', 'ccclt-send-info-popup__info-dialog__icon');
  infoDialog.appendChild(infoDialogIcon);

  let infoDialogMsg = document.createElement('p');
  infoDialogMsg.setAttribute('id', 'ccclt-send-info-popup__info-dialog__message');
  infoDialog.appendChild(infoDialogMsg);

  let infoDialogCloseButton = document.createElement('div');
  infoDialogCloseButton.setAttribute('id', 'ccclt-send-info-popup__info-dialog__close-button');
  infoDialogCloseButton.innerHTML = 'Đóng';
  infoDialog.addEventListener('click', () => {
    hideInfoDialog();
  })
  infoDialog.appendChild(infoDialogCloseButton);

  //Popup header
  let header = document.createElement('div');
  header.setAttribute('id', 'ccclt-send-info-popup__header');
  popup.appendChild(header);

  let closeButton = document.createElement('div');
  closeButton.setAttribute('id', 'ccclt-send-info-popup__close-button');
  closeButton.innerHTML = 'x';
  closeButton.addEventListener('click', () => {
    hideSendInfoPopup();
  });
  header.appendChild(closeButton);

  //Popup content
  let content = document.createElement('div');
  content.setAttribute('id', 'ccclt-send-info-popup__content');
  popup.appendChild(content);

  let form = document.createElement('form');
  form.setAttribute('id', 'ccclt-send-info-popup__form');
  form.setAttribute('method', 'POST');
  content.appendChild(form);

  let formTitle = document.createElement('div');
  formTitle.setAttribute('id', 'ccclt-send-info-popup__form__title');
  formTitle.innerHTML = 'Trợ giúp qua điện thoại';
  formTitle.style.color = data.popupConfig.themeColor;
  form.appendChild(formTitle);

  let formNotice = document.createElement('div');
  formNotice.setAttribute('id', 'ccclt-send-info-popup__form__notice');
  formNotice.innerHTML = 'Vui lòng nhập họ tên, số điện thoại và email của bạn, chúng tôi sẽ gọi lại bạn ngay. Hoặc bạn có thể  ';
  form.appendChild(formNotice);

  let formNoticeContact = document.createElement('a');
  formNoticeContact.setAttribute('class', 'ccclt-send-info-popup__href-link');
  formNoticeContact.href = `tel:${data.popupConfig.supporter.phone}`;
  formNoticeContact.innerHTML = 'nhấn vào đây';
  formNoticeContact.style.color = data.popupConfig.themeColor;
  formNotice.appendChild(formNoticeContact);

  let formNoticeText = document.createElement('span');
  formNoticeText.innerHTML = ' để gọi trực tiếp cho chúng tôi.';
  formNotice.appendChild(formNoticeText);

  let formInput1 = document.createElement('input');
  formInput1.setAttribute('id', 'ccclt-send-info-popup__form__input1');
  formInput1.setAttribute('class', 'ccclt-send-info-popup__form__input');
  formInput1.setAttribute('placeholder', 'Họ và tên');
  form.appendChild(formInput1);

  let formInput2 = document.createElement('input');
  formInput2.setAttribute('id', 'ccclt-send-info-popup__form__input2');
  formInput2.setAttribute('class', 'ccclt-send-info-popup__form__input');
  formInput2.setAttribute('placeholder', 'Số điện thoại');
  formInput2.addEventListener('keypress', e => {
    const keyCode = e.keyCode;
    if ((keyCode < 48 || keyCode > 57) && keyCode !== 45 && keyCode !== 13) {
      e.preventDefault();
    }
  });
  form.appendChild(formInput2);

  let formInput3 = document.createElement('input');
  formInput3.setAttribute('id', 'ccclt-send-info-popup__form__input3');
  formInput3.setAttribute('class', 'ccclt-send-info-popup__form__input');
  formInput3.setAttribute('placeholder', 'Địa chỉ email');
  form.appendChild(formInput3);

  let sendInfoButton = document.createElement('button');
  sendInfoButton.setAttribute('id', 'ccclt-send-info-popup__send-info-button');
  sendInfoButton.setAttribute('type', 'button');
  sendInfoButton.style.backgroundColor = data.popupConfig.themeColor;
  sendInfoButton.innerHTML = 'Gửi yêu cầu';
  form.appendChild(sendInfoButton);
  sendInfoButton.addEventListener('click', e => {
    e.preventDefault();
    const name = document.getElementById('ccclt-send-info-popup__form__input1').value;
    const phone = document.getElementById('ccclt-send-info-popup__form__input2').value;
    const email = document.getElementById('ccclt-send-info-popup__form__input3').value;

    if (!name || !phone) {
      showInfoDialog('Vui lòng điền đầy đủ thông tin', 400);
      return false;
    }

    const phoneNumberPattern = /((09|03|07|08|05)+([0-9]{8})\b)/g;

    if (!phoneNumberPattern.test(phone)) {
      showInfoDialog('Số điện thoại không hợp lệ', 400);
      return false;
    }

    let customerInfo = {
      name,
      phoneNumber: phone,
      domain: window.location.origin,
      key: CONFIG.key
    }

    if (email) {
      const emailPattern = /^[a-z][a-z0-9_\.]{5,32}@[a-z0-9]{2,}(\.[a-z0-9]{2,4}){1,2}$/g;

      if (!emailPattern.test(email)) {
        showInfoDialog('Email không hợp lệ', 400);
        return false;
      }

      customerInfo['email'] = email;
    }

    let customerUuid = localStorage.getItem('uuid');
    const addCustomerInfoUrl = APIs.addCustomerInfo.replace(':uuid', customerUuid);

    if(localStorage.getItem('gclid')){
      customerInfo['gclid'] = localStorage.getItem('gclid');
    }

    $.post(addCustomerInfoUrl, customerInfo, (data, status) => {
      if (status == 'success') {
        showInfoDialog('Cảm ơn bạn đã cung cấp thông tin! Chúng tôi sẽ gọi lại bạn ngay.', 200);
        clearFormFields();
        return true;
      }

      showInfoDialog('Gửi thông tin thất bại.', 400);
      return false;
    });
  });

  let notification = document.createElement('p');
  notification.setAttribute('id', 'ccclt-send-info-popup__notification');
  form.appendChild(notification);

  let supporterInfo = document.createElement('div');
  supporterInfo.setAttribute('id', 'ccclt-send-info-popup__supporter__info');
  content.appendChild(supporterInfo);

  let supporterAvatar = document.createElement('img');
  supporterAvatar.setAttribute('id', 'ccclt-send-info-popup__supporter__avatar');
  supporterAvatar.src = data.popupConfig.supporter.avatar;
  supporterInfo.appendChild(supporterAvatar);

  let supporterName = document.createElement('div');
  supporterName.setAttribute('id', 'ccclt-send-info-popup__supporter__name');
  supporterName.innerHTML = data.popupConfig.supporter.name;
  supporterInfo.appendChild(supporterName);

  let supporterMajor = document.createElement('div');
  supporterMajor.setAttribute('id', 'ccclt-send-info-popup__supporter__major');
  supporterMajor.innerHTML = data.popupConfig.supporter.major;
  supporterInfo.appendChild(supporterMajor);

  let supporterPhoneButton = document.createElement('a');
  supporterPhoneButton.setAttribute('id', 'ccclt-send-info-popup__supporter__phone-button');
  supporterPhoneButton.href = `tel:${data.popupConfig.supporter.phone}`;
  supporterInfo.appendChild(supporterPhoneButton);

  let supporterPhoneIcon = document.createElement('img');
  supporterPhoneIcon.setAttribute('class', 'ccclt-send-info-popup__icon');
  supporterPhoneIcon.src = 'https://ducanhduhoc.vn/wp-content/themes/zen/asset/img/phone-flat.png';
  supporterPhoneButton.appendChild(supporterPhoneIcon);

  let supportPhoneNumber = document.createElement('span');
  supportPhoneNumber.setAttribute('id', 'ccclt-send-info-popup__supporter__phone-number');
  supportPhoneNumber.innerHTML = data.popupConfig.supporter.phone ? numberWithSpaces(data.popupConfig.supporter.phone, '### ### ####') : '';
  supporterPhoneButton.appendChild(supportPhoneNumber);

  let clearDiv = document.createElement('div');
  clearDiv.setAttribute('class', 'ccclt-send-info-popup__clear');
  content.appendChild(clearDiv);

  let footer = document.createElement('div');
  footer.setAttribute('id', 'ccclt-send-info-popup__footer');
  let footerIcon = document.createElement('img');
  footerIcon.src = 'https://image.flaticon.com/icons/png/128/222/222506.png';
  footer.appendChild(footerIcon);
  let footerTitle = document.createElement('span');
  footerTitle.innerHTML = 'x2.com.vn';
  footer.appendChild(footerTitle);
  content.appendChild(footer);

  // Open popup button
  let openPopupButton = document.createElement('div');
  openPopupButton.setAttribute('id', 'ccclt-send-info-popup__open-popup-button');

  if (!data.popupConfig.popupPosition || data.popupConfig.popupPosition === 2)
    openPopupButton.setAttribute('class', 'ccclt-send-info-popup__bottom-right');
  if (data.popupConfig.popupPosition === 1)
    openPopupButton.setAttribute('class', 'ccclt-send-info-popup__bottom-left');
  if (data.popupConfig.popupPosition === 3)
    openPopupButton.setAttribute('class', 'ccclt-send-info-popup__center-right');
  if (data.popupConfig.popupPosition === 4)
    openPopupButton.setAttribute('class', 'ccclt-send-info-popup__center-left');
  if (data.popupConfig.popupPosition === 5)
    openPopupButton.setAttribute('class', 'ccclt-send-info-popup__top-left');
  if (data.popupConfig.popupPosition === 6)
    openPopupButton.setAttribute('class', 'ccclt-send-info-popup__top-right');

  openPopupButton.style.backgroundImage = `url(${data.popupConfig.supporter.avatar})`;
  openPopupButton.addEventListener('click', () => {
    if (container.style.display !== 'block')
      showSendInfoPopup();
  });
  let sayHelloLabel = document.createElement('div');
  sayHelloLabel.setAttribute('id', 'ccclt-send-info-popup__say-hello-label');
  sayHelloLabel.innerHTML = 'Hi';
  openPopupButton.appendChild(sayHelloLabel);
  document.body.appendChild(openPopupButton);

  document.addEventListener('keypress', e => {
    if (e.keyCode === 13) {
      if (document.getElementById('ccclt-send-info-popup__info-dialog__container').style.display === 'flex')
        hideInfoDialog();
      else sendInfoButton.click();
    }
  });
};

const clearFormFields = () => {
  const formInputs = document.getElementsByClassName('ccclt-send-info-popup__form__input');
  if (formInputs.length > 0) {
    for (let input of formInputs) {
      input.value = '';
    }
  }
  formInputs[0].focus();
};

const clearForm = () => {
  //clearFormFields();
  document.getElementById('ccclt-send-info-popup__notification').innerHTML = '';
};

const showSendInfoPopup = () => {
  document.getElementById('ccclt-send-info-popup__container').style.display = 'flex';
  document.getElementById('ccclt-send-info-popup__form__input1').focus();
};

const hideSendInfoPopup = () => {
  clearForm();
  hideInfoDialog();
  document.getElementById('ccclt-send-info-popup__container').style.display = 'none';
};

const showInfoDialog = (msg, status) => {
  document.getElementById('ccclt-send-info-popup__info-dialog__icon').src = '';
  if (status === 200) {
    document.getElementById('ccclt-send-info-popup__info-dialog__icon')
      .src = 'https://images.vexels.com/media/users/3/157890/isolated/lists/4f2c005416b7f48b3d6d09c5c6763d87-check-mark-circle-icon.png';
  } else {
    document.getElementById('ccclt-send-info-popup__info-dialog__icon')
      .src = 'http://media3.scdn.vn/img2/2017/4_11/z9fcMd.png';
  }
  document.getElementById('ccclt-send-info-popup__info-dialog__message').innerHTML = msg;
  document.getElementById('ccclt-send-info-popup__info-dialog__container').style.display = 'flex';
}

const hideInfoDialog = () => {
  document.getElementById('ccclt-send-info-popup__info-dialog__container').style.display = 'none';
}

const numberWithSpaces = (value, pattern) => {
  let i = 0, phone = value.toString();
  return pattern.replace(/#/g, _ => phone[i++]);
};