const initFakeCustomer = async (dataSend) => {
  if (dataSend) {
    if (dataSend.data) {
      if (dataSend.data.fakeCustomerConfig) {
        $(document).ready(() => {
          const data = dataSend.data.fakeCustomerConfig;

          if(data.isEnabled)
          {
            loadFakeCustomer(data);
          }
        });

        return;
      }
      return;
    }
    return;
  }
  return;
};

const loadFakeCustomer = data => {
  let container = document.createElement('div');
  container.setAttribute('id', 'ccclt-fake-customer');
  container.style.borderLeft = '4px solid ' + data.themeColor;
  
  const shape = data.shape || 1;
  if(shape === 1){
    container.style.borderRadius = 0;
  }
  if(shape === 2){
    container.style.borderRadius = '5px';
  }

  document.body.appendChild(container);

  hideFakeCustomer();

  let pos;
  let positionOnPage = data.positionOnPage || 1;

  if (positionOnPage == 1)
    pos = 'ccclt-fake-customer__bottom-left';
  if (positionOnPage == 2)
    pos = 'ccclt-fake-customer__bottom-right';
  if (positionOnPage == 3)
    pos = 'ccclt-fake-customer__top-left';
  if (positionOnPage == 4)
    pos = 'ccclt-fake-customer__top-right';
  container.setAttribute('class', pos);

  let avatar = document.createElement('div');
  avatar.setAttribute('id', 'ccclt-fake-customer__avatar');
  container.appendChild(avatar);

  let avatarImg = document.createElement('img');
  avatarImg.src = '';
  avatar.appendChild(avatarImg);

  let info = document.createElement('div');
  info.setAttribute('id', 'ccclt-fake-customer__info');
  container.appendChild(info);

  let infoTitle = document.createElement('div');
  infoTitle.setAttribute('id', 'ccclt-fake-customer__info__title');
  info.appendChild(infoTitle);

  let infoBody = document.createElement('div');
  infoBody.setAttribute('id', 'ccclt-fake-customer__info__body');
  info.appendChild(infoBody);

  let infoPageUrl = document.createElement('div');
  infoPageUrl.setAttribute('id', 'ccclt-fake-customer__info__page-url');
  info.appendChild(infoPageUrl);

  let infoPageUrlLink = document.createElement('a');
  infoPageUrlLink.href = data.pageUrl;
  infoPageUrlLink.innerHTML = data.pageUrl;
  infoPageUrlLink.style.textDecoration = 'none';
  infoPageUrlLink.target = '_blank';
  infoPageUrlLink.style.color = data.themeColor;
  infoPageUrl.appendChild(infoPageUrlLink);

  let footer = document.createElement('div');
  footer.setAttribute('id', 'ccclt-fake-customer__footer');
  container.appendChild(footer);

  let footerIcon = document.createElement('img');
  footerIcon.src = 'https://image.flaticon.com/icons/png/128/222/222506.png';
  footer.appendChild(footerIcon);

  let footerTitle = document.createElement('span');
  footerTitle.innerHTML = 'x2.com.vn';
  footer.appendChild(footerTitle);

  let closeButton = document.createElement('span');
  closeButton.setAttribute('id', 'ccclt-fake-customer__close-button');
  closeButton.innerHTML = 'x';
  closeButton.addEventListener('click', () => { hideFakeCustomer() });
  container.appendChild(closeButton);

  const timeToHide = 5000, timeToDisplay = data.autoDisplayTime;
  setTimeToDisplay(timeToDisplay, timeToHide, data);
};

const getListAvatars = (data) => {
  let avatars = [];
  let avatarType = data.avatarType || 1;

  switch (avatarType) {
    //teenagers
    case 1:
      for (let i = 110; i <= 199; i++) {
        avatars.push(`https://w.cokhach.com/assets/images/avatar/2/${i}.jpg`);
      }
      break;

    //matures
    case 2:
      for (let i = 111; i <= 198; i++) {
        avatars.push(`https://w.cokhach.com/assets/images/avatar/3/${i}.jpg`);
      }
      break;

    //symbols
    case 3:
      for (let i = 111; i <= 129; i++) {
        avatars.push(`https://w.cokhach.com/assets/images/avatar/1/${i}.png`);
      }
      break;

    case 4:
      for (let i = 1; i <= 16; i++) {
        avatars.push(`https://w.cokhach.com/assets/images/avatar/4/${i}.gif`);
      }
      break;
    default:
      break;
  }

  return avatars;
};

const setTimeToDisplay = (timeToDisplay, timeToHide, data) => {
  let rand = 10 * 1000;
  let oldRand = 0;
  let max = 90;
  let min = 10;

  if(timeToDisplay)
  {
    if(timeToDisplay.length >= 2)
    {
      if(timeToDisplay[0] >= timeToDisplay[1])
      {
        max = timeToDisplay[0];
        min = timeToDisplay[1];
      }
      else{
        max = timeToDisplay[1];
        min = timeToDisplay[0];
      }
    }
  }

  showFakeCustomer(data);
  setTimeout(() => {
    hideFakeCustomer();
  }, timeToHide);

  setInterval(() => {
    oldRand = rand;
    rand = (Math.round(Math.random() * (max - min)) + min) * 1050;
    rand += oldRand - 10000;

    setTimeout(() => {  showFakeCustomer(data);
      setTimeout(() => {
        hideFakeCustomer();
      }, timeToHide);
    }, rand);
  }, 10000);
};

const hideFakeCustomer = () => {
  document.getElementById('ccclt-fake-customer').style.display = 'none';
};

const showFakeCustomer = (data) => {
  let avatars = getListAvatars(data);
  let randomIndex = Math.floor(Math.random() * avatars.length);
  const avatar = document.getElementById('ccclt-fake-customer__avatar');
  avatar.childNodes[0].src = avatars[randomIndex];
  let title = data.title || 'Mới mua hàng!';
  let body = data.body || 'Khách hàng #fake_email vừa mới mua hàng thành công!';
  $.get(APIs.getCustomerData, (customerData, status) => {
    processData(customerData, title, body);
  });
};

const processData = (customerData, title, body) => {
  const name = customerData.nameAndEmail['F.Name'] + ' ' + customerData.nameAndEmail['L.Name'];
  const email = customerData.nameAndEmail['Email'];
  const location = customerData.address['Address'];
  const phoneNumber = customerData.phoneNumber['Mobile'];
  const mapData = {
    name,
    email,
    location,
    phoneNumber
  };

  title = mapFakeData(title, mapData);
  body = mapFakeData(body, mapData);

  document.getElementById('ccclt-fake-customer__info__title').innerHTML = title;
  document.getElementById('ccclt-fake-customer__info__body').innerHTML = body;
  document.getElementById('ccclt-fake-customer').style.display = 'flex';
};

const mapFakeData = (text, customerData) => {
  const mappedText = text
    .replace(/#fake_name/g, customerData.name)
    .replace(/#fake_phone/g, customerData.phoneNumber)
    .replace(/#fake_email/g, customerData.email)
    .replace(/#fake_location/g, customerData.location);

  return mappedText;
};