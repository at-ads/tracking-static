const log = async () => {
  try {
    if (window.location.href == oldUrl) {
      return;
    }
    // set referrer
    let referrer = oldUrl;

    // assign current url to oldUrl
    oldUrl = window.location.href;
    const userAgent = window.navigator.userAgent;
    const href = window.location.href;

    browserResolution.width = window.outerWidth;
    browserResolution.height = window.outerHeight;
    let uuid = CONFIG.uuid;

    if (typeof Storage !== 'undefined') {
      if (!localStorage.getItem('uuid')) {
        localStorage.setItem('uuid', uuid);
      } else {
        uuid = localStorage.getItem('uuid');
      }
    }

    let info = {
      ip,
      key: CONFIG.key,
      uuid,
      href,
      location: userLocation,
      referrer,
      userAgent,
      isPrivateBrowsing,
      browserResolution,
      screenResolution,
      createdAt: new Date().getTime()
    };

    const resMsisdn = await fetch(APIs.getMsisdn);
    const msisdnData = await resMsisdn.json();

    if(msisdnData.msisdn)
    {
      info.msisdn = msisdnData.msisdn;
    }

    let json = JSON.stringify(info);

    const res = await fetch(APIs.log, {
      method: 'post',
      credentials: 'include',
      headers: {
        'Content-type': 'application/json'
      },
      body: json
    });
    const data = await res.json();
    logId = data.data.logId;
    console.log(data.messages.join('\n'));
  } catch (e) {
    console.log(e);
  }
};