const getGeoIp = async () => {
  const res = await fetch('https://geoip-db.com/json/');
  const data = await res.json();
  ip = data.IPv4;
  userLocation = data;
  delete userLocation.IPv4;
};