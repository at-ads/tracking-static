const APIs = {
  log: CONFIG.hostApi + '/api/user-behaviors/log',
  timeUnload: CONFIG.hostApi + '/api/user-behaviors/log/:id/time-unload',
  scrollPercentage: CONFIG.hostApi + '/api/user-behaviors/log/:id/scroll-percentage',
  checkPopupOfWebsite: CONFIG.hostApi + '/api/websites/:key/popup',
  addCustomerInfo: CONFIG.hostApi + '/api/customer-infomation/:uuid',
  getMsisdn: CONFIG.hostTracking + '/static/msisdn',
  getCustomerData: CONFIG.hostTracking + '/static/customer-data',
  updateLog: CONFIG.hostApi + '/api/user-behaviors/log/',
};