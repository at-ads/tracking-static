const CONFIG = {
  hostApi: '<%= hostApi %>',
  uuid: '<%= uuid %>',
  key: '<%= key %>',
  scriptTracking: '<%= scriptTracking %>',
  hostTracking: '<%= hostTracking %>'
};