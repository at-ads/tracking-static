// TODO: should create function encode body before send
'use strict';
let countScriptTracking = document.querySelectorAll(
  `script[src*='${CONFIG.scriptTracking}']`
).length;
let isValidToTracking = countScriptTracking === 1 ? true : false;

const urlTracking = CONFIG.hostTracking;

// Send Info Popup Style
let sendInfoPopupStyle = document.createElement('link');  
sendInfoPopupStyle.rel = 'stylesheet';    
sendInfoPopupStyle.type = 'text/css'; 
sendInfoPopupStyle.href = urlTracking + '/static/css/send-info-popup.style.css'; 
document.getElementsByTagName('HEAD')[0].appendChild(sendInfoPopupStyle);

// Fake Customer Style
let fakeCustomerStyle = document.createElement('link');  
fakeCustomerStyle.rel = 'stylesheet';    
fakeCustomerStyle.type = 'text/css'; 
fakeCustomerStyle.href = urlTracking + '/static/css/fake-customer.style.css'; 
document.getElementsByTagName('HEAD')[0].appendChild(fakeCustomerStyle);

let ip = '';
let isPrivateBrowsing = false;
let oldUrl = window.document.referrer;
let userAgent = '';
let userLocation = null;
let logId = null;
let newScroll = 0;
let oldScroll = 0;
const intervalTime = 3000; // ms

let browserResolution = {
  width: window.outerWidth,
  height: window.outerHeight
};

let screenResolution = {
  width: screen.width,
  height: screen.height
};

const init = async () => {
  // if (typeof Storage !== 'undefined') {
  //   localStorage.setItem('logId', 123);
  // }
  const url = new URL(window.location.href);
  const click_gclid = url.searchParams.get("click_gclid");
  let uuid = CONFIG.uuid;

  if (typeof Storage !== 'undefined') {
    if (!localStorage.getItem('uuid')) {
      localStorage.setItem('uuid', uuid);
    } 
    // else {
    //   uuid = localStorage.getItem('uuid');
    // }

    if(click_gclid)
    {
      if (!localStorage.getItem('gclid') || localStorage.getItem('gclid') != click_gclid) {
        localStorage.setItem('gclid', click_gclid);
      }

      if (window.location.href == oldUrl) {
        return;
      }

      await checkPrivate();
      let referrer = oldUrl;
      oldUrl = window.location.href;
      const href = window.location.href;
      browserResolution.width = window.outerWidth;
      browserResolution.height = window.outerHeight;
      screenResolution.width = screen.width;
      screenResolution.height = screen.height;

      setTimeout(()=> {
        const body = {
          uuid: localStorage.getItem('uuid'),
          gclid: click_gclid,
          referrer,
          browserResolution,
          screenResolution,
          href,
          isPrivateBrowsing
        };

        $.ajax({
          contentType: 'application/json',
          dataType   : 'json',
          type: 'PUT',
          async: true,
          url: APIs.updateLog,
          data: JSON.stringify(body),
          success: (result) => {
            console.log('thành công');
          }
        })
      }, 1000)
    }
  }

  let urlCheckwebsite = APIs.checkPopupOfWebsite.replace(':key', CONFIG.key);
  urlCheckwebsite = urlCheckwebsite + `?domain=${window.location.origin}`;
  const res = await fetch(urlCheckwebsite);
  const dataSend = await res.json();

  await initSendInfoPopup(dataSend);
  await initFakeCustomer(dataSend);

  // setInterval(() => {
  //   getScrollPercentage();
  //   detectScroll();
  // }, 1000);

  // // get ip.
  // await getGeoIp();
  // // get is Private Browsing.
  // await checkPrivate();
  // //get userAgent.
  // userAgent = window.navigator.userAgent;

  // window.onunload = function(e) {
  //   e.preventDefault();
  //   onUnload(e);
  //   return false;
  // };

  // log();
  // setInterval(onUnload, intervalTime); // interval check time on page
};

loadMultiFiles(
  [
    {filename: 'https://cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js', filetype: 'js'},
    window.$ ? null : {filename: 'https://code.jquery.com/jquery-1.12.4.min.js', filetype: 'js'},
  ]
).then(init);