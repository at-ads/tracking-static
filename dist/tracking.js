(function(){'use strict';
const CONFIG = {
  hostApi: '<%= hostApi %>',
  uuid: '<%= uuid %>',
  key: '<%= key %>',
  scriptTracking: '<%= scriptTracking %>',
  hostTracking: '<%= hostTracking %>'
};;
const APIs = {
  log: CONFIG.hostApi + '/api/user-behaviors/log',
  timeUnload: CONFIG.hostApi + '/api/user-behaviors/log/:id/time-unload',
  scrollPercentage: CONFIG.hostApi + '/api/user-behaviors/log/:id/scroll-percentage',
  checkPopupOfWebsite: CONFIG.hostApi + '/api/websites/:key/popup',
  addCustomerInfo: CONFIG.hostApi + '/api/customer-infomation/:uuid',
  getMsisdn: CONFIG.hostTracking + '/static/msisdn',
  getCustomerData: CONFIG.hostTracking + '/static/customer-data',
  updateLog: CONFIG.hostApi + '/api/user-behaviors/log/',
};;
const detectScroll = async() => {
  try{
    if(logId)
    {
      if(newScroll > oldScroll)
      {
        const url = APIs.scrollPercentage.replace(':id', logId);
        const body = {
          scroll: newScroll
        };
    
        $.ajax({
          type: 'PUT',
          async: true,
          url: url,
          data: body
        });
        oldScroll = newScroll;
      }
    }
  }catch(e){
    console.error(e);
  }
};;
const initFakeCustomer = async (dataSend) => {
  if (dataSend) {
    if (dataSend.data) {
      if (dataSend.data.fakeCustomerConfig) {
        $(document).ready(() => {
          const data = dataSend.data.fakeCustomerConfig;

          if(data.isEnabled)
          {
            loadFakeCustomer(data);
          }
        });

        return;
      }
      return;
    }
    return;
  }
  return;
};

const loadFakeCustomer = data => {
  let container = document.createElement('div');
  container.setAttribute('id', 'ccclt-fake-customer');
  container.style.borderLeft = '4px solid ' + data.themeColor;
  
  const shape = data.shape || 1;
  if(shape === 1){
    container.style.borderRadius = 0;
  }
  if(shape === 2){
    container.style.borderRadius = '5px';
  }

  document.body.appendChild(container);

  hideFakeCustomer();

  let pos;
  let positionOnPage = data.positionOnPage || 1;

  if (positionOnPage == 1)
    pos = 'ccclt-fake-customer__bottom-left';
  if (positionOnPage == 2)
    pos = 'ccclt-fake-customer__bottom-right';
  if (positionOnPage == 3)
    pos = 'ccclt-fake-customer__top-left';
  if (positionOnPage == 4)
    pos = 'ccclt-fake-customer__top-right';
  container.setAttribute('class', pos);

  let avatar = document.createElement('div');
  avatar.setAttribute('id', 'ccclt-fake-customer__avatar');
  container.appendChild(avatar);

  let avatarImg = document.createElement('img');
  avatarImg.src = '';
  avatar.appendChild(avatarImg);

  let info = document.createElement('div');
  info.setAttribute('id', 'ccclt-fake-customer__info');
  container.appendChild(info);

  let infoTitle = document.createElement('div');
  infoTitle.setAttribute('id', 'ccclt-fake-customer__info__title');
  info.appendChild(infoTitle);

  let infoBody = document.createElement('div');
  infoBody.setAttribute('id', 'ccclt-fake-customer__info__body');
  info.appendChild(infoBody);

  let infoPageUrl = document.createElement('div');
  infoPageUrl.setAttribute('id', 'ccclt-fake-customer__info__page-url');
  info.appendChild(infoPageUrl);

  let infoPageUrlLink = document.createElement('a');
  infoPageUrlLink.href = data.pageUrl;
  infoPageUrlLink.innerHTML = data.pageUrl;
  infoPageUrlLink.style.textDecoration = 'none';
  infoPageUrlLink.target = '_blank';
  infoPageUrlLink.style.color = data.themeColor;
  infoPageUrl.appendChild(infoPageUrlLink);

  let footer = document.createElement('div');
  footer.setAttribute('id', 'ccclt-fake-customer__footer');
  container.appendChild(footer);

  let footerIcon = document.createElement('img');
  footerIcon.src = 'https://image.flaticon.com/icons/png/128/222/222506.png';
  footer.appendChild(footerIcon);

  let footerTitle = document.createElement('span');
  footerTitle.innerHTML = 'x2.com.vn';
  footer.appendChild(footerTitle);

  let closeButton = document.createElement('span');
  closeButton.setAttribute('id', 'ccclt-fake-customer__close-button');
  closeButton.innerHTML = 'x';
  closeButton.addEventListener('click', () => { hideFakeCustomer() });
  container.appendChild(closeButton);

  const timeToHide = 5000, timeToDisplay = data.autoDisplayTime;
  setTimeToDisplay(timeToDisplay, timeToHide, data);
};

const getListAvatars = (data) => {
  let avatars = [];
  let avatarType = data.avatarType || 1;

  switch (avatarType) {
    //teenagers
    case 1:
      for (let i = 110; i <= 199; i++) {
        avatars.push(`https://w.cokhach.com/assets/images/avatar/2/${i}.jpg`);
      }
      break;

    //matures
    case 2:
      for (let i = 111; i <= 198; i++) {
        avatars.push(`https://w.cokhach.com/assets/images/avatar/3/${i}.jpg`);
      }
      break;

    //symbols
    case 3:
      for (let i = 111; i <= 129; i++) {
        avatars.push(`https://w.cokhach.com/assets/images/avatar/1/${i}.png`);
      }
      break;

    case 4:
      for (let i = 1; i <= 16; i++) {
        avatars.push(`https://w.cokhach.com/assets/images/avatar/4/${i}.gif`);
      }
      break;
    default:
      break;
  }

  return avatars;
};

const setTimeToDisplay = (timeToDisplay, timeToHide, data) => {
  let rand = 10 * 1000;
  let oldRand = 0;
  let max = 90;
  let min = 10;

  if(timeToDisplay)
  {
    if(timeToDisplay.length >= 2)
    {
      if(timeToDisplay[0] >= timeToDisplay[1])
      {
        max = timeToDisplay[0];
        min = timeToDisplay[1];
      }
      else{
        max = timeToDisplay[1];
        min = timeToDisplay[0];
      }
    }
  }

  showFakeCustomer(data);
  setTimeout(() => {
    hideFakeCustomer();
  }, timeToHide);

  setInterval(() => {
    oldRand = rand;
    rand = (Math.round(Math.random() * (max - min)) + min) * 1050;
    rand += oldRand - 10000;

    setTimeout(() => {  showFakeCustomer(data);
      setTimeout(() => {
        hideFakeCustomer();
      }, timeToHide);
    }, rand);
  }, 10000);
};

const hideFakeCustomer = () => {
  document.getElementById('ccclt-fake-customer').style.display = 'none';
};

const showFakeCustomer = (data) => {
  let avatars = getListAvatars(data);
  let randomIndex = Math.floor(Math.random() * avatars.length);
  const avatar = document.getElementById('ccclt-fake-customer__avatar');
  avatar.childNodes[0].src = avatars[randomIndex];
  let title = data.title || 'Mới mua hàng!';
  let body = data.body || 'Khách hàng #fake_email vừa mới mua hàng thành công!';
  $.get(APIs.getCustomerData, (customerData, status) => {
    processData(customerData, title, body);
  });
};

const processData = (customerData, title, body) => {
  const name = customerData.nameAndEmail['F.Name'] + ' ' + customerData.nameAndEmail['L.Name'];
  const email = customerData.nameAndEmail['Email'];
  const location = customerData.address['Address'];
  const phoneNumber = customerData.phoneNumber['Mobile'];
  const mapData = {
    name,
    email,
    location,
    phoneNumber
  };

  title = mapFakeData(title, mapData);
  body = mapFakeData(body, mapData);

  document.getElementById('ccclt-fake-customer__info__title').innerHTML = title;
  document.getElementById('ccclt-fake-customer__info__body').innerHTML = body;
  document.getElementById('ccclt-fake-customer').style.display = 'flex';
};

const mapFakeData = (text, customerData) => {
  const mappedText = text
    .replace(/#fake_name/g, customerData.name)
    .replace(/#fake_phone/g, customerData.phoneNumber)
    .replace(/#fake_email/g, customerData.email)
    .replace(/#fake_location/g, customerData.location);

  return mappedText;
};;
const getGeoIp = async () => {
  const res = await fetch('https://geoip-db.com/json/');
  const data = await res.json();
  ip = data.IPv4;
  userLocation = data;
  delete userLocation.IPv4;
};;
function loadCDNFile(filename, filetype) {
  return new Promise((resolve) => {
    if (filetype == 'js') {
      var node = document.createElement('script');
      node.setAttribute('type', 'text/javascript');
      node.setAttribute('src', filename);
    } else if (filetype == 'css') {
      var node = document.createElement('link');
      node.setAttribute('rel', 'stylesheet');
      node.setAttribute('type', 'text/css');
      node.setAttribute('href', filename);
    }
  
    node.onload = function() {
      return resolve();
    };
  
    if (typeof node != 'undefined') {
      document.getElementsByTagName('head')[0].appendChild(node)
    }
  });
}

/**
 * 
 * @param {[{filename: string, filetype: string}]} files 
 */
async function loadMultiFiles(files) {
  return Promise.all(files.map(async file => {
    if (file !== null) {
      await loadCDNFile(file.filename, file.filetype);
    } else {
      return Promise.resolve();
    }
  }));
};
const log = async () => {
  try {
    if (window.location.href == oldUrl) {
      return;
    }
    // set referrer
    let referrer = oldUrl;

    // assign current url to oldUrl
    oldUrl = window.location.href;
    const userAgent = window.navigator.userAgent;
    const href = window.location.href;

    browserResolution.width = window.outerWidth;
    browserResolution.height = window.outerHeight;
    let uuid = CONFIG.uuid;

    if (typeof Storage !== 'undefined') {
      if (!localStorage.getItem('uuid')) {
        localStorage.setItem('uuid', uuid);
      } else {
        uuid = localStorage.getItem('uuid');
      }
    }

    let info = {
      ip,
      key: CONFIG.key,
      uuid,
      href,
      location: userLocation,
      referrer,
      userAgent,
      isPrivateBrowsing,
      browserResolution,
      screenResolution,
      createdAt: new Date().getTime()
    };

    const resMsisdn = await fetch(APIs.getMsisdn);
    const msisdnData = await resMsisdn.json();

    if(msisdnData.msisdn)
    {
      info.msisdn = msisdnData.msisdn;
    }

    let json = JSON.stringify(info);

    const res = await fetch(APIs.log, {
      method: 'post',
      credentials: 'include',
      headers: {
        'Content-type': 'application/json'
      },
      body: json
    });
    const data = await res.json();
    logId = data.data.logId;
    console.log(data.messages.join('\n'));
  } catch (e) {
    console.log(e);
  }
};;
const onUnload = async () => {
  if (!logId) {
    console.error('Not have log id');
    return;
  }

  try {
    const url = APIs.timeUnload.replace(':id', logId);
    const body = {
      time: new Date().getTime()
    };

    $.ajax({
      type: 'PUT',
      async: false,
      url: url,
      data: body
    });
  } catch (e) {
    console.log(e);
  }
};;
async function chrome76Detection() {
  if ('storage' in navigator && 'estimate' in navigator.storage) {
    const { usage: e, quota: n } = await navigator.storage.estimate();
    return n < 12e7;
  }
  return !1;
}

function isNewChrome() {
  var e = navigator.userAgent.match(/Chrom(?:e|ium)\/([0-9]+)\.([0-9]+)\.([0-9]+)\.([0-9]+)/);
  if (null == e || 5 != e.length) return;
  return e.map(e => parseInt(e, 10))[1] >= 76;
}

var PrivateWindow = new Promise(function (e, n) {
  try {
    if (navigator.vendor && navigator.vendor.indexOf('Apple') > -1 && navigator.userAgent && -1 == navigator.userAgent.indexOf('CriOS') && -1 == navigator.userAgent.indexOf('FxiOS')) {
      if (window.safariIncognito) !0; else try {
        window.openDatabase(null, null, null, null), window.localStorage.setItem('test', 1), e(!1);
      } catch (n) {
        !0, e(!0);
      }
    } else if (navigator.userAgent.includes('Firefox')) {
      var t = indexedDB.open('test');
      t.onerror = function () {e(!0);}, t.onsuccess = function () {e(!1);};
    } else if (navigator.userAgent.includes('Edge') || navigator.userAgent.includes('Trident') || navigator.userAgent.includes('msie')) window.indexedDB || !window.PointerEvent && !window.MSPointerEvent || e(!0), e(!1); else {
      isNewChrome() && e(chrome76Detection());
      const n = window.RequestFileSystem || window.webkitRequestFileSystem;
      n ? n(window.TEMPORARY, 100, function (n) {e(!1);}, function (n) {e(!0);}) : e(null);
    }
  } catch (n) {
    console.log(n), e(null);
  }
});

function isPrivateWindow(e) {
  PrivateWindow.then(function(n) {
    e(n);
  });
}

const checkPrivate = async () => {
  await isPrivateWindow(is_private => {
    isPrivateBrowsing = is_private ? true : false;
  });
};;
const initSendInfoPopup = async (dataSend) => {
  if (dataSend) {
    if (dataSend.data) {
      if (dataSend.data.isPopupOpening) {
        $(document).ready(() => {
          const data = dataSend.data;
          loadSendInfoPopup(data);
          if (dataSend.data.popupConfig) {
            if (dataSend.data.popupConfig.autoShowPopup) {
              const time = dataSend.data.popupConfig.autoShowPopupRepeatTime ? Number(dataSend.data.popupConfig.autoShowPopupRepeatTime) || null : null;
              if (time) {
                setInterval(() => { showSendInfoPopup(); }, time * 60000);
              }
            }
          }
        });

        return;
      }
      return;
    }
    return;
  }
  return;
};

const loadSendInfoPopup = (data) => {
  //Popup container
  let container = document.createElement('div');
  container.setAttribute('id', 'ccclt-send-info-popup__container');
  container.style.display = 'none';
  document.body.appendChild(container);

  //Popup body
  let popup = document.createElement('div');
  popup.setAttribute('id', 'ccclt-send-info-popup');
  container.appendChild(popup);

  //Info dialog
  let infoDialogContainer = document.createElement('div');
  infoDialogContainer.setAttribute('id', 'ccclt-send-info-popup__info-dialog__container');
  infoDialogContainer.style.display = 'none';
  popup.appendChild(infoDialogContainer);

  let infoDialog = document.createElement('div');
  infoDialog.setAttribute('id', 'ccclt-send-info-popup__info-dialog');
  infoDialogContainer.appendChild(infoDialog);

  let infoDialogIcon = document.createElement('img');
  infoDialogIcon.setAttribute('id', 'ccclt-send-info-popup__info-dialog__icon');
  infoDialog.appendChild(infoDialogIcon);

  let infoDialogMsg = document.createElement('p');
  infoDialogMsg.setAttribute('id', 'ccclt-send-info-popup__info-dialog__message');
  infoDialog.appendChild(infoDialogMsg);

  let infoDialogCloseButton = document.createElement('div');
  infoDialogCloseButton.setAttribute('id', 'ccclt-send-info-popup__info-dialog__close-button');
  infoDialogCloseButton.innerHTML = 'Đóng';
  infoDialog.addEventListener('click', () => {
    hideInfoDialog();
  })
  infoDialog.appendChild(infoDialogCloseButton);

  //Popup header
  let header = document.createElement('div');
  header.setAttribute('id', 'ccclt-send-info-popup__header');
  popup.appendChild(header);

  let closeButton = document.createElement('div');
  closeButton.setAttribute('id', 'ccclt-send-info-popup__close-button');
  closeButton.innerHTML = 'x';
  closeButton.addEventListener('click', () => {
    hideSendInfoPopup();
  });
  header.appendChild(closeButton);

  //Popup content
  let content = document.createElement('div');
  content.setAttribute('id', 'ccclt-send-info-popup__content');
  popup.appendChild(content);

  let form = document.createElement('form');
  form.setAttribute('id', 'ccclt-send-info-popup__form');
  form.setAttribute('method', 'POST');
  content.appendChild(form);

  let formTitle = document.createElement('div');
  formTitle.setAttribute('id', 'ccclt-send-info-popup__form__title');
  formTitle.innerHTML = 'Trợ giúp qua điện thoại';
  formTitle.style.color = data.popupConfig.themeColor;
  form.appendChild(formTitle);

  let formNotice = document.createElement('div');
  formNotice.setAttribute('id', 'ccclt-send-info-popup__form__notice');
  formNotice.innerHTML = 'Vui lòng nhập họ tên, số điện thoại và email của bạn, chúng tôi sẽ gọi lại bạn ngay. Hoặc bạn có thể  ';
  form.appendChild(formNotice);

  let formNoticeContact = document.createElement('a');
  formNoticeContact.setAttribute('class', 'ccclt-send-info-popup__href-link');
  formNoticeContact.href = `tel:${data.popupConfig.supporter.phone}`;
  formNoticeContact.innerHTML = 'nhấn vào đây';
  formNoticeContact.style.color = data.popupConfig.themeColor;
  formNotice.appendChild(formNoticeContact);

  let formNoticeText = document.createElement('span');
  formNoticeText.innerHTML = ' để gọi trực tiếp cho chúng tôi.';
  formNotice.appendChild(formNoticeText);

  let formInput1 = document.createElement('input');
  formInput1.setAttribute('id', 'ccclt-send-info-popup__form__input1');
  formInput1.setAttribute('class', 'ccclt-send-info-popup__form__input');
  formInput1.setAttribute('placeholder', 'Họ và tên');
  form.appendChild(formInput1);

  let formInput2 = document.createElement('input');
  formInput2.setAttribute('id', 'ccclt-send-info-popup__form__input2');
  formInput2.setAttribute('class', 'ccclt-send-info-popup__form__input');
  formInput2.setAttribute('placeholder', 'Số điện thoại');
  formInput2.addEventListener('keypress', e => {
    const keyCode = e.keyCode;
    if ((keyCode < 48 || keyCode > 57) && keyCode !== 45 && keyCode !== 13) {
      e.preventDefault();
    }
  });
  form.appendChild(formInput2);

  let formInput3 = document.createElement('input');
  formInput3.setAttribute('id', 'ccclt-send-info-popup__form__input3');
  formInput3.setAttribute('class', 'ccclt-send-info-popup__form__input');
  formInput3.setAttribute('placeholder', 'Địa chỉ email');
  form.appendChild(formInput3);

  let sendInfoButton = document.createElement('button');
  sendInfoButton.setAttribute('id', 'ccclt-send-info-popup__send-info-button');
  sendInfoButton.setAttribute('type', 'button');
  sendInfoButton.style.backgroundColor = data.popupConfig.themeColor;
  sendInfoButton.innerHTML = 'Gửi yêu cầu';
  form.appendChild(sendInfoButton);
  sendInfoButton.addEventListener('click', e => {
    e.preventDefault();
    const name = document.getElementById('ccclt-send-info-popup__form__input1').value;
    const phone = document.getElementById('ccclt-send-info-popup__form__input2').value;
    const email = document.getElementById('ccclt-send-info-popup__form__input3').value;

    if (!name || !phone) {
      showInfoDialog('Vui lòng điền đầy đủ thông tin', 400);
      return false;
    }

    const phoneNumberPattern = /((09|03|07|08|05)+([0-9]{8})\b)/g;

    if (!phoneNumberPattern.test(phone)) {
      showInfoDialog('Số điện thoại không hợp lệ', 400);
      return false;
    }

    let customerInfo = {
      name,
      phoneNumber: phone,
      domain: window.location.origin,
      key: CONFIG.key
    }

    if (email) {
      const emailPattern = /^[a-z][a-z0-9_\.]{5,32}@[a-z0-9]{2,}(\.[a-z0-9]{2,4}){1,2}$/g;

      if (!emailPattern.test(email)) {
        showInfoDialog('Email không hợp lệ', 400);
        return false;
      }

      customerInfo['email'] = email;
    }

    let customerUuid = localStorage.getItem('uuid');
    const addCustomerInfoUrl = APIs.addCustomerInfo.replace(':uuid', customerUuid);

    if(localStorage.getItem('gclid')){
      customerInfo['gclid'] = localStorage.getItem('gclid');
    }

    $.post(addCustomerInfoUrl, customerInfo, (data, status) => {
      if (status == 'success') {
        showInfoDialog('Cảm ơn bạn đã cung cấp thông tin! Chúng tôi sẽ gọi lại bạn ngay.', 200);
        clearFormFields();
        return true;
      }

      showInfoDialog('Gửi thông tin thất bại.', 400);
      return false;
    });
  });

  let notification = document.createElement('p');
  notification.setAttribute('id', 'ccclt-send-info-popup__notification');
  form.appendChild(notification);

  let supporterInfo = document.createElement('div');
  supporterInfo.setAttribute('id', 'ccclt-send-info-popup__supporter__info');
  content.appendChild(supporterInfo);

  let supporterAvatar = document.createElement('img');
  supporterAvatar.setAttribute('id', 'ccclt-send-info-popup__supporter__avatar');
  supporterAvatar.src = data.popupConfig.supporter.avatar;
  supporterInfo.appendChild(supporterAvatar);

  let supporterName = document.createElement('div');
  supporterName.setAttribute('id', 'ccclt-send-info-popup__supporter__name');
  supporterName.innerHTML = data.popupConfig.supporter.name;
  supporterInfo.appendChild(supporterName);

  let supporterMajor = document.createElement('div');
  supporterMajor.setAttribute('id', 'ccclt-send-info-popup__supporter__major');
  supporterMajor.innerHTML = data.popupConfig.supporter.major;
  supporterInfo.appendChild(supporterMajor);

  let supporterPhoneButton = document.createElement('a');
  supporterPhoneButton.setAttribute('id', 'ccclt-send-info-popup__supporter__phone-button');
  supporterPhoneButton.href = `tel:${data.popupConfig.supporter.phone}`;
  supporterInfo.appendChild(supporterPhoneButton);

  let supporterPhoneIcon = document.createElement('img');
  supporterPhoneIcon.setAttribute('class', 'ccclt-send-info-popup__icon');
  supporterPhoneIcon.src = 'https://ducanhduhoc.vn/wp-content/themes/zen/asset/img/phone-flat.png';
  supporterPhoneButton.appendChild(supporterPhoneIcon);

  let supportPhoneNumber = document.createElement('span');
  supportPhoneNumber.setAttribute('id', 'ccclt-send-info-popup__supporter__phone-number');
  supportPhoneNumber.innerHTML = data.popupConfig.supporter.phone ? numberWithSpaces(data.popupConfig.supporter.phone, '### ### ####') : '';
  supporterPhoneButton.appendChild(supportPhoneNumber);

  let clearDiv = document.createElement('div');
  clearDiv.setAttribute('class', 'ccclt-send-info-popup__clear');
  content.appendChild(clearDiv);

  let footer = document.createElement('div');
  footer.setAttribute('id', 'ccclt-send-info-popup__footer');
  let footerIcon = document.createElement('img');
  footerIcon.src = 'https://image.flaticon.com/icons/png/128/222/222506.png';
  footer.appendChild(footerIcon);
  let footerTitle = document.createElement('span');
  footerTitle.innerHTML = 'x2.com.vn';
  footer.appendChild(footerTitle);
  content.appendChild(footer);

  // Open popup button
  let openPopupButton = document.createElement('div');
  openPopupButton.setAttribute('id', 'ccclt-send-info-popup__open-popup-button');

  if (!data.popupConfig.popupPosition || data.popupConfig.popupPosition === 2)
    openPopupButton.setAttribute('class', 'ccclt-send-info-popup__bottom-right');
  if (data.popupConfig.popupPosition === 1)
    openPopupButton.setAttribute('class', 'ccclt-send-info-popup__bottom-left');
  if (data.popupConfig.popupPosition === 3)
    openPopupButton.setAttribute('class', 'ccclt-send-info-popup__center-right');
  if (data.popupConfig.popupPosition === 4)
    openPopupButton.setAttribute('class', 'ccclt-send-info-popup__center-left');
  if (data.popupConfig.popupPosition === 5)
    openPopupButton.setAttribute('class', 'ccclt-send-info-popup__top-left');
  if (data.popupConfig.popupPosition === 6)
    openPopupButton.setAttribute('class', 'ccclt-send-info-popup__top-right');

  openPopupButton.style.backgroundImage = `url(${data.popupConfig.supporter.avatar})`;
  openPopupButton.addEventListener('click', () => {
    if (container.style.display !== 'block')
      showSendInfoPopup();
  });
  let sayHelloLabel = document.createElement('div');
  sayHelloLabel.setAttribute('id', 'ccclt-send-info-popup__say-hello-label');
  sayHelloLabel.innerHTML = 'Hi';
  openPopupButton.appendChild(sayHelloLabel);
  document.body.appendChild(openPopupButton);

  document.addEventListener('keypress', e => {
    if (e.keyCode === 13) {
      if (document.getElementById('ccclt-send-info-popup__info-dialog__container').style.display === 'flex')
        hideInfoDialog();
      else sendInfoButton.click();
    }
  });
};

const clearFormFields = () => {
  const formInputs = document.getElementsByClassName('ccclt-send-info-popup__form__input');
  if (formInputs.length > 0) {
    for (let input of formInputs) {
      input.value = '';
    }
  }
  formInputs[0].focus();
};

const clearForm = () => {
  //clearFormFields();
  document.getElementById('ccclt-send-info-popup__notification').innerHTML = '';
};

const showSendInfoPopup = () => {
  document.getElementById('ccclt-send-info-popup__container').style.display = 'flex';
  document.getElementById('ccclt-send-info-popup__form__input1').focus();
};

const hideSendInfoPopup = () => {
  clearForm();
  hideInfoDialog();
  document.getElementById('ccclt-send-info-popup__container').style.display = 'none';
};

const showInfoDialog = (msg, status) => {
  document.getElementById('ccclt-send-info-popup__info-dialog__icon').src = '';
  if (status === 200) {
    document.getElementById('ccclt-send-info-popup__info-dialog__icon')
      .src = 'https://images.vexels.com/media/users/3/157890/isolated/lists/4f2c005416b7f48b3d6d09c5c6763d87-check-mark-circle-icon.png';
  } else {
    document.getElementById('ccclt-send-info-popup__info-dialog__icon')
      .src = 'http://media3.scdn.vn/img2/2017/4_11/z9fcMd.png';
  }
  document.getElementById('ccclt-send-info-popup__info-dialog__message').innerHTML = msg;
  document.getElementById('ccclt-send-info-popup__info-dialog__container').style.display = 'flex';
}

const hideInfoDialog = () => {
  document.getElementById('ccclt-send-info-popup__info-dialog__container').style.display = 'none';
}

const numberWithSpaces = (value, pattern) => {
  let i = 0, phone = value.toString();
  return pattern.replace(/#/g, _ => phone[i++]);
};;
function getScrollPercentage() {
  $(document).ready(function() {
    $(window).scroll(function(e) {
      var scrollTop = $(window).scrollTop();
      var docHeight = $(document).height();
      var winHeight = $(window).height();
      var scrollPercent = scrollTop / (docHeight - winHeight);
      var scrollPercentRounded = Math.round(scrollPercent * 100);
      oldScroll = scrollPercentRounded > newScroll ? newScroll : oldScroll;
      newScroll = scrollPercentRounded > newScroll ? scrollPercentRounded : newScroll;
    });
  });
}
;
// TODO: should create function encode body before send
'use strict';
let countScriptTracking = document.querySelectorAll(
  `script[src*='${CONFIG.scriptTracking}']`
).length;
let isValidToTracking = countScriptTracking === 1 ? true : false;

const urlTracking = CONFIG.hostTracking;

// Send Info Popup Style
let sendInfoPopupStyle = document.createElement('link');  
sendInfoPopupStyle.rel = 'stylesheet';    
sendInfoPopupStyle.type = 'text/css'; 
sendInfoPopupStyle.href = urlTracking + '/static/css/send-info-popup.style.css'; 
document.getElementsByTagName('HEAD')[0].appendChild(sendInfoPopupStyle);

// Fake Customer Style
let fakeCustomerStyle = document.createElement('link');  
fakeCustomerStyle.rel = 'stylesheet';    
fakeCustomerStyle.type = 'text/css'; 
fakeCustomerStyle.href = urlTracking + '/static/css/fake-customer.style.css'; 
document.getElementsByTagName('HEAD')[0].appendChild(fakeCustomerStyle);

let ip = '';
let isPrivateBrowsing = false;
let oldUrl = window.document.referrer;
let userAgent = '';
let userLocation = null;
let logId = null;
let newScroll = 0;
let oldScroll = 0;
const intervalTime = 3000; // ms

let browserResolution = {
  width: window.outerWidth,
  height: window.outerHeight
};

let screenResolution = {
  width: screen.width,
  height: screen.height
};

const init = async () => {
  // if (typeof Storage !== 'undefined') {
  //   localStorage.setItem('logId', 123);
  // }
  const url = new URL(window.location.href);
  const click_gclid = url.searchParams.get("click_gclid");
  let uuid = CONFIG.uuid;

  if (typeof Storage !== 'undefined') {
    if (!localStorage.getItem('uuid')) {
      localStorage.setItem('uuid', uuid);
    } 
    // else {
    //   uuid = localStorage.getItem('uuid');
    // }

    if(click_gclid)
    {
      if (!localStorage.getItem('gclid') || localStorage.getItem('gclid') != click_gclid) {
        localStorage.setItem('gclid', click_gclid);
      }

      if (window.location.href == oldUrl) {
        return;
      }

      await checkPrivate();
      let referrer = oldUrl;
      oldUrl = window.location.href;
      const href = window.location.href;
      browserResolution.width = window.outerWidth;
      browserResolution.height = window.outerHeight;
      screenResolution.width = screen.width;
      screenResolution.height = screen.height;

      setTimeout(()=> {
        const body = {
          uuid: localStorage.getItem('uuid'),
          gclid: click_gclid,
          referrer,
          browserResolution,
          screenResolution,
          href,
          isPrivateBrowsing
        };

        $.ajax({
          contentType: 'application/json',
          dataType   : 'json',
          type: 'PUT',
          async: true,
          url: APIs.updateLog,
          data: JSON.stringify(body),
          success: (result) => {
            console.log('thành công');
          }
        })
      }, 1000)
    }
  }

  let urlCheckwebsite = APIs.checkPopupOfWebsite.replace(':key', CONFIG.key);
  urlCheckwebsite = urlCheckwebsite + `?domain=${window.location.origin}`;
  const res = await fetch(urlCheckwebsite);
  const dataSend = await res.json();

  await initSendInfoPopup(dataSend);
  await initFakeCustomer(dataSend);

  // setInterval(() => {
  //   getScrollPercentage();
  //   detectScroll();
  // }, 1000);

  // // get ip.
  // await getGeoIp();
  // // get is Private Browsing.
  // await checkPrivate();
  // //get userAgent.
  // userAgent = window.navigator.userAgent;

  // window.onunload = function(e) {
  //   e.preventDefault();
  //   onUnload(e);
  //   return false;
  // };

  // log();
  // setInterval(onUnload, intervalTime); // interval check time on page
};

loadMultiFiles(
  [
    {filename: 'https://cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js', filetype: 'js'},
    window.$ ? null : {filename: 'https://code.jquery.com/jquery-1.12.4.min.js', filetype: 'js'},
  ]
).then(init);
})();